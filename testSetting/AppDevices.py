__author__ = 'shikun'
# -*- coding:utf-8 -*-
import configparser
from testMode.BaseAppDevices import *
from testData.Global import *
def setAppDevices():
    config = configparser.ConfigParser()
    config.read(r'D:\app\appium_study\AppDevices.ini')
    deviceName = config['DEFAULT']['devices']
    platformName = config["DEFAULT"]['platformName']
    platformVersion = config['DEFAULT']['platformVersion']
    appPackage = config['DEFAULT']['appPackage']
    common.PACKAGE = appPackage
    appActivity = config['DEFAULT']['appActivity']
    Remote = config['DEFAULT']['Remote']
    appiumJs = config['DEFAULT']['appiumjs']

    gd = getDriver({"deviceName": deviceName, "platformName": platformName, "platformVersion": platformVersion, "appPackage": appPackage,
                    "appActivity": appActivity, "Remote": Remote, "appiumJs": appiumJs})
    return gd
